<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyercalendars', function (Blueprint $table) {
            $table->id();
            $table->integer('lawyer_id');
            $table->date ('date');
            $table->time('from');
            $table->time('to');
            $table->tinyInteger('del');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyercalendars');
    }
};

?>

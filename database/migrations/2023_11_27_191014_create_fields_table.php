<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->string('field_name');
            $table->string('description');
            $table->timestamps();
        });

        $arr= ['name','middlename','lastname', 'country', 'governorate','address', 'office_name','practice_area', 
        'phone_number', 'office_number','longitude', 'latitude', 'img', 'dates', 'price', 'ability'];

        for( $i=0; $i<16;$i++ ){
            DB::table('fields')->insert(
                array(
                    'field_name' => 'f'.$i,
                    'description' => $arr[$i]
                )
            );
        }
        
    }
  
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
};

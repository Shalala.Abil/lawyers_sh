<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_areas', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        $arr = [
            'Employment Law',
            'Dispute Resolution',
            'Criminal Law',
            'Construction Law',
            'Competition Law',
            'Clinical Negligence Law'
        ];

        for ($i = 1; $i < 6; $i++) {
            DB::table('practice_areas')->insert(
                array(
                    'name' => $arr[$i]
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practice_areas');
    }
};

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminController extends Controller
{
    //
    public function index(){
        $users=User::all()->toarray();

       return  view('admin.index', ['users' => $users]);
    }

    public function freeze($id){
        User::where('id', $id)
        ->update(['froozen' => 1]);
    }

    public function delete($id){
        
        User::where('id', $id)
        ->update(['del' => 1]);

    }
}

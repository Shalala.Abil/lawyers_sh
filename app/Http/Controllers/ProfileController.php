<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


use App\Models\Country;
use App\Models\PracticeAreas;
use App\Models\ProfileInfo;
use App\Models\User;

use App\Models\Fields;




class ProfileController extends Controller
{

    public function index(){


        $data= ProfileInfo::with('user')->get()->toArray();
        $arr=[];

        foreach($data as $k=>$v){  
            $arr[$v['field']]=$v['data'];
        }    


        return view('profile', ['data' => $arr]);


    }
    public function create()
    {

        $data['countries'] = Country::all('name', 'code');
        $data['practice_areas'] = PracticeAreas::all('id', 'name');


        return view('create-profile', ['data' => $data]);
    }

    public function store(Request $request)
    {
        // dd( Auth::user()->id);
        // $request->validate([
        //     'name' => 'required',
        //     'middlename' => 'required',
        //     'lastname' => 'required',
        //     'country' => 'required',
        //     'governorate' => 'required',
        //     'address' => 'required',
        //     'office_name' => 'required',
        //     'practice_area' => 'required',
        //     'phone_number' => 'required',
        //     'office_number' => 'required',
        //     'longitude' => 'required',
        //     'latitude' => 'required',
        //     'img' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        //     'dates' => 'required',
        //     'price' => 'required',
        //     'ability' => 'required'
        // ]);


        $fields = Fields::all('id', 'field_name', 'description')->toArray();


        $image_path = $request->file('img')->store('image', 'public');
        // dd($image_path);


        foreach ($fields as $field) {

            $d = $field['description'];

            $data = $request->$d;

            if ($field['field_name'] == 'f12') $data = $image_path;

            // echo ($d . "      " . $data . "<br>");

            $user[] = ProfileInfo::create([
                'field' => $field['field_name'],
                'data' => $data,
                'del' => 0,
                'user_id' => Auth::user()->id

            ]);


        }





        // event(new Registered($user));
        // $credentials = $request->only('email', 'password');

        // Auth::attempt($credentials);
        // $request->session()->regenerate();
        // return redirect()->route('verification.notice');
    }

    //
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lawyercalendar extends Model
{
    use HasFactory;

    protected $fillable = ['lawyer_id', 'date','from','to','del'];
}

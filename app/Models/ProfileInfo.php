<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileInfo extends Model
{
    use HasFactory;


    
    protected $fillable = [
        "user_id",
        "field",
        "data",
        "del"
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}

<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Auth\LoginRegisterController;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CalendarController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/calendar', function(){
    return view('calendar');
})->name('calendar') ;

Route::get('/fullcalender', [CalendarController::class, 'index'])->name('fullcalender') ;
Route::post('/fullcalenderAjax', [CalendarController::class, 'ajax'])->name('fullcalenderAjax') ;




Route::get('/home', [HomeController::class, 'index'])->name('home') ;
Route::get('/adminpanel', [AdminController::class, 'index'])->name('admin') ;

Route::get('/freeze/{id}', [AdminController::class, 'freeze'])->name('freeze') ;
Route::get('/delete/{id}', [AdminController::class, 'delete'])->name('delete') ;



Route::get('/create/profile', [ProfileController::class, 'create'])->name('create.profile') ;
Route::get('/show/profile', [ProfileController::class, 'index'])->name('show.profile') ;

Route::post('/save/profile', [ProfileController::class, 'store'])->name('save.profile') ;



// Define Custom User Registration & Login Routes
Route::controller(LoginRegisterController::class)->group(function() {
    Route::get('/register{role}', 'register')->name('register');
    Route::post('/store', 'store')->name('store');
    Route::get('/login', 'login')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
    Route::post('/logout', 'logout')->name('logout');
});

// Define Custom Verification Routes
Route::controller(VerificationController::class)->group(function() {
    Route::get('/email/verify', 'notice')->name('verification.notice');
    Route::get('/email/verify/{id}/{hash}', 'verify')->name('verification.verify');
    Route::post('/email/resend', 'resend')->name('verification.resend');
});

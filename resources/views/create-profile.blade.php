<style>
    .scroll {
        max-height: 1000px;
        overflow-y: auto;
    }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
<!-- MDB -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/7.0.0/mdb.min.css" rel="stylesheet" />


<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    rel="stylesheet">

<!-- Other CSS and meta tags -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


@extends('layouts.master')

@section('content')





<Section class="" style="height: 1000px; margin-top:100px;">
    <div class="container py-5">
        <!-- <div class="row">
      <div class="col">
        <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">User</a></li>
            <li class="breadcrumb-item active" aria-current="page">User Profile</li>
          </ol>
        </nav>
      </div>
    </div> -->





        <div class="row">
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="scroll card-body text-center">
                        <!-- <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                            alt="avatar" class="rounded-circle img-fluid" style="width: 150px;"> -->
                        <h2>Create your profile</h2>
                        <form action="{{ route('save.profile') }}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group mb-3">

                                <input type="text " placeholder="name"
                                    class="form-control @error('name ') is-invalid @enderror" id="name" name="name"
                                    value="{{ old('name ') }}">
                                @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group mb-3">
                                <input type="text" placeholder="middlename"
                                    class="form-control @error('middlename') is-invalid @enderror" id="middlename"
                                    name="middlename">
                                @if ($errors->has('middlename'))
                                <span class="text-danger">{{ $errors->first('middlename') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="lastname"
                                    class="form-control @error('lastname') is-invalid @enderror" id="lastname"
                                    name="lastname">
                                @if ($errors->has('lastname'))
                                <span class="text-danger">{{ $errors->first('lastname') }}</span>
                                @endif
                            </div>


                            <div class="form-group mb-3">

                                <select class="form-select" aria-label="Default select example" id="country"
                                    name="country">
                                    <option selected value="none">Select country</option>

                                    @foreach ($data['countries'] as $country)

                                    <option value="{{$country['code']}}">{{
                                        $country['code']."-".$country['name']}}</option>
                                    @endforeach

                                </select>

                            </div>




                            <div class="form-group mb-3">
                                <input type="text" placeholder="governorate"
                                    class="form-control @error('governorate') is-invalid @enderror" id="governorate"
                                    name="governorate">
                                @if ($errors->has('governorate'))
                                <span class="text-danger">{{ $errors->first('governorate') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="address"
                                    class="form-control @error('address') is-invalid @enderror" id="address"
                                    name="address">
                                @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="office_name"
                                    class="form-control @error('office_name') is-invalid @enderror" id="office_name"
                                    name="office_name">
                                @if ($errors->has('office_name'))
                                <span class="text-danger">{{ $errors->first('office_name') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <select class="form-select" id="practice_area" name="practice_area">
                                    <option selected value="none">Select practice ares</option>

                                    @foreach ($data['practice_areas'] as $area)
                                    <option value="{{$area['id']}}">{{ $area['name']}}</option>
                                    @endforeach

                                </select>

                            </div>


                            <div class="form-group mb-3">
                                <input type="text" placeholder="phone_number"
                                    class="form-control @error('phone_number') is-invalid @enderror" id="phone_number"
                                    name="phone_number">
                                @if ($errors->has('phone_number'))
                                <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                                @endif
                            </div>


                            <div class="form-group mb-3">
                                <input type="text" placeholder="office_number"
                                    class="form-control @error('office_number') is-invalid @enderror" id="office_number"
                                    name="office_number">
                                @if ($errors->has('office_number'))
                                <span class="text-danger">{{ $errors->first('office_number') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="longitude"
                                    class="form-control @error('longitude') is-invalid @enderror" id="longitude"
                                    name="longitude">
                                @if ($errors->has('longitude'))
                                <span class="text-danger">{{ $errors->first('longitude') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="latitude"
                                    class="form-control @error('latitude') is-invalid @enderror" id="latitude"
                                    name="latitude">
                                @if ($errors->has('latitude'))
                                <span class="text-danger">{{ $errors->first('longitude') }}</span>
                                @endif
                            </div>



                            <div class="form-group mb-3">


                                <input type="file"
                                    class="form-control-file block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4
                                     file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100"
                                    name="img" id="img">

                                @if ($errors->has('img'))
                                <span class="text-danger">{{ $errors->first('img') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="datepicker" class="form-control" id="dates" name="dates"
                                    autocomplete="off">

                                @if ($errors->has('dates'))
                                <span class="text-danger">{{ $errors->first('dates') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <input type="text" placeholder="price"
                                    class="form-control @error('price') is-invalid @enderror" id="price" name="price">
                                @if ($errors->has('price'))
                                <span class="text-danger">{{ $errors->first('price') }}</span>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <select class="form-select" id="ability" name="ability" id="ability" name="ability">
                                    <option selected value="none">Select ability</option>

                                    <option value="yes">Yes</option>
                                    <option value="no">no</option>
                                </select>

                            </div>

                            <div class="d-grid mx-auto">
                                <input type="submit" class="col-md-3 offset-md-5 btn btn-primary" value="Save">
                            </div>



                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






@endsection


<script>        $(d        ent).ready(funct           {
        tepicker').dat            r({
            multidate: true,
        format: 'yyyy-m            // Customize the date f      t as neede     // Y        an a     mor based on your requirements
    });
    });
</script>
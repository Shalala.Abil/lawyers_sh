@include('admin.layouts.header')
@include('admin.layouts.sidebar')

@yield('title')

<body class="hidden-bar-wrapper">

<div class="page-wrapper">
 	
    @yield('content')

</div>
@include('admin.layouts.footer')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<style>
    body {
        background: rgb(99, 39, 120)
    }

    .form-control:focus {
        box-shadow: none;
        border-color: #BA68C8
    }

    .profile-button {
        background: rgb(99, 39, 120);
        box-shadow: none;
        border: none
    }

    .profile-button:hover {
        background: #682773
    }

    .profile-button:focus {
        background: #682773;
        box-shadow: none
    }

    .profile-button:active {
        background: #682773;
        box-shadow: none
    }

    .back:hover {
        color: #682773;
        cursor: pointer
    }

    .labels {
        font-size: 11px
    }

    .add-experience:hover {
        background: #BA68C8;
        color: #fff;
        cursor: pointer;
        border: solid 1px #BA68C8
    }


</style>
<div class="container rounded bg-white mt-5 mb-5">
    <div class="row">
        <div class="col-md-3 border-right">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img class="rounded-circle mt-5"
                    width="150px"
                    src="{{asset('storage/'.$data['f12'])}}"

                    src="profile.jpg"><span
                    class="font-weight-bold">{{Auth::user()->name}}</span><span
                    class="text-black-50">{{Auth::user()->email}}</span><span>
                </span></div>
        </div>
        <div class="col-md-9 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Profile Settings</h4>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4"><label class="labels">Name</label><input type="text" class="form-control"
                            placeholder="first name" value="{{$data['f0']}}"></div>
                    <div class="col-md-4"><label class="labels">Middle name</label><input type="text"
                            class="form-control" value="{{$data['f1']}}" placeholder="surname"></div>
                    <div class="col-md-4"><label class="labels">Surname</label><input type="text" class="form-control"
                            value="{{$data['f2']}}" placeholder="surname"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12"><label class="labels">country</label><input type="text" class="form-control"
                            placeholder="enter phone number" value="{{$data['f3']}}"></div>
                    <div class="col-md-12"><label class="labels">address</label><input type="text" class="form-control"
                            placeholder="enter address line 1" value="{{$data['f4']}}"></div>
                    <div class="col-md-12"><label class="labels">office_name</label><input type="text"
                            class="form-control" placeholder="enter address line 2" value="{{$data['f5']}}"></div>
                    <div class="col-md-12"><label class="labels">practice_area</label><input type="text"
                            class="form-control" placeholder="enter address line 2" value="{{$data['f6']}}"></div>
                    <div class="col-md-12"><label class="labels">phone_number</label><input type="text"
                            class="form-control" placeholder="enter address line 2" value="{{$data['f7']}}"></div>
                    <div class="col-md-12"><label class="labels">office_number</label><input type="text"
                            class="form-control" placeholder="enter address line 2" value="{{$data['f8']}}"></div>
                    <div class="col-md-12"><label class="labels">longitude</label><input type="text"
                            class="form-control" placeholder="enter email id" value="{{$data['f10']}}"></div>
                    <div class="col-md-12"><label class="labels">latitude</label><input type="text" class="form-control"
                            placeholder="education" value="{{$data['f11']}}"></div>
                    <div class="col-md-12"><label class="labels">dates</label><input type="text" class="form-control"
                            placeholder="education" value="{{$data['f13']}}"></div>
                    <div class="col-md-12"><label class="labels">price</label><input type="text" class="form-control"
                            placeholder="education" value="{{$data['f14']}}"></div>
                    <div class="col-md-12"><label class="labels">ability</label><input type="text" class="form-control"
                            placeholder="education" value="{{$data['f15']}}"></div>

                </div>

                <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Save
                        Profile</button></div>
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center experience"><span>Edit
                        Experience</span><span class="border px-3 p-1 add-experience"><i
                            class="fa fa-plus"></i>&nbsp;Experience</span></div><br>
                <div class="col-md-12"><label class="labels">Experience in Designing</label><input type="text"
                        class="form-control" placeholder="experience" value=""></div> <br>
                <div class="col-md-12"><label class="labels">Additional Details</label><input type="text"
                        class="form-control" placeholder="additional details" value=""></div>
            </div>
        </div> -->
    </div>
</div>
</div>
</div>
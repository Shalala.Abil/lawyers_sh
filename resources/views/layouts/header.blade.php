<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Epasos</title>
	<!-- Stylesheets -->


	<link href="{{asset('front/assets')}}/css/bootstrap.css" rel="stylesheet">
	<link href="{{asset('front/assets')}}/css/style.css" rel="stylesheet">
	<link href="{{asset('front/assets')}}/css/responsive.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
		rel="stylesheet">

	<link rel="shortcut icon" href="{{asset('front/assets')}}/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="{{asset('front/assets')}}/images/favicon.png" type="image/x-icon">

	<!-- Responsive -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
	<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>




<!-- Header Style One -->
<header class="main-header">

	<!-- Header Top -->
	<div class="header-top">
		<div class="auto-container">
			<div class="inner-container clearfix">
				<!-- Top Left -->
				<div class="top-left clearfix">
					<div class="text"><span>Working time:</span> Monday to Friday 9 AM - 5 PM</div>
				</div>

				<!-- Top Right -->
				<div class="top-right pull-right clearfix">
					<!-- Social Box -->
					<ul class="social-box">
						<li><a href="https://www.facebook.com/" class="fa fa-facebook-f"></a></li>
						<li><a href="https://www.twitter.com/" class="fa fa-twitter"></a></li>
						<li><a href="https://www.behance.com/" class="fa fa-behance"></a></li>
						<li><a href="https://www.linkedin.com/" class="fa fa-linkedin"></a></li>
						<li><a href="https://youtube.com/" class="fa fa-youtube-play"></a></li>
					</ul>
					<div class="text">Call for free consultation: <a href="tel:+0056-693-55-20">0056 693 55 20</a></div>
				</div>

			</div>
		</div>
	</div>

	<!-- Header Lower -->
	<div class="header-lower"  style="background-color:green;">
		<div class="auto-container clearfix">

			<!-- Logo Box -->
			<div class="pull-left logo-box">
				<div class="logo"><a href="index.html"><img src="{{asset('front/assets')}}/images/logo.png" alt=""
							title=""></a></div>
			</div>

			<!-- Nav Outer -->
			<div class="nav-outer clearfix">
				<!-- Mobile Navigation Toggler -->
				<div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>
				<!-- Main Menu -->
				<nav class="main-menu navbar-expand-md">
					<div class="navbar-header">
						<!-- Toggle Button -->
						<button class="navbar-toggler" type="button" data-toggle="collapse"
							data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
							aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
						<ul class="navigation clearfix">

							@guest
							<li class="nav-item">
								<a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}"
									href="{{ route('login') }}">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}"
									href="{{ route('register', ['role' => 0]) }}">Be User</a>
							</li>
							<li class="nav-item">
								<a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}"
									href="{{ route('register', ['role' => 1]) }}">Be Lawyer</a>
							</li>
							@else
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
									aria-expanded="false">
									{{ Auth::user()->name }}
								</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Logout</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST">
											@csrf
										</form>
									</li>
								</ul>
							</li>

							<li class="nav-item">
								<a class="nav-link "
									href="{{ route('create.profile')}}">Create Profile</a>
							</li>

							<li class="nav-item">
								<a class="nav-link "
									href="{{ route('admin')}}">Admin</a>
							</li>

							<li class="nav-item">
								<a class="nav-link "
									href="{{ route('show.profile')}}">My Profile</a>
							</li>

							@endguest

						</ul>

						<!-- <ul>
										<li><a href="index.html">Homepage One</a></li>
										<li><a href="index-2.html">Homepage Two</a></li>
										<li><a href="index-3.html">Homepage Three</a></li>
										<li class="dropdown"><a href="#">Header Styles</a>
											<ul>
												<li><a href="index.html">Header Style One</a></li>
												<li><a href="index-2.html">Header Style Two</a></li>
												<li><a href="index-3.html">Header Style Three</a></li>
											</ul>
										</li>
									</ul> 
								</li>
								<li class="dropdown"><a href="#">About</a>
									 <ul>
										<li><a href="about.html">About us</a></li>
										<li><a href="faq.html">Faq's</a></li>
										<li><a href="price.html">Price</a></li>
										<li class="dropdown"><a href="#">Our Team</a>
											<ul>
												<li><a href="mentors.html">Team</a></li>
												<li><a href="mentor-detail.html">Team Detail</a></li>
											</ul>
										</li>
									</ul> 
								</li>
								<li class="dropdown"><a href="#">Services</a>
									<ul>
										<li><a href="services.html">Services</a></li>
										<li><a href="service-detail.html">Services Detail</a></li>
									</ul> 
								</li>
								<li class="dropdown"><a href="#">Projects</a>
									<ul>
										<li><a href="portfolio.html">Projects</a></li>
										<li><a href="portfolio-detail.html">Projects Detail</a></li>
									</ul> 
								</li>
								<li class="dropdown"><a href="#">Courses</a>
									 <ul>
										<li><a href="courses.html">Courses</a></li>
										<li><a href="course-detail.html">Courses Detail</a></li>
									</ul> 
								</li>
								<li class="dropdown"><a href="#">Blog</a>
									 <ul>
										<li><a href="blog.html">Our Blog</a></li>
										<li><a href="blog-detail.html">Blog Detail</a></li>
									</ul> 
								</li>
								<li><a href="contact.html">Contact</a></li>


							</ul>

							-->

					</div>
				</nav>

				<!-- Main Menu End-->
				<div class="outer-box clearfix">

					<!-- Search Btn -->
					<div class="search-box-btn transition-300ms"><span class="icon fa fa-search"></span></div>

					<!-- Nav Btn -->
					<div class="nav-btn navSidebar-button transition-300ms"><span class="icon flaticon-menu-1"></span>
					</div>

				</div>
			</div>

		</div>
	</div>
	<!-- End Header Lower -->

	<!-- Sticky Header  -->
	<div class="sticky-header">
		<div class="auto-container clearfix">
			<!--Logo-->
			<div class="logo pull-left">
				<a href="index.html" title=""><img src="{{asset('front/assets')}}/images/logo-small.png" alt=""
						title=""></a>
			</div>
			<!--Right Col-->
			<div class="pull-right">

				<!-- Main Menu -->
				<nav class="main-menu">
					<!--Keep This Empty / Menu will come through Javascript-->
				</nav>
				<!-- Main Menu End-->

				<!-- Mobile Navigation Toggler -->
				<div class="mobile-nav-toggler"><span class="icon flaticon-menu-1"></span></div>

			</div>
		</div>
	</div><!-- End Sticky Menu -->

	<!-- Mobile Menu  -->
	<div class="mobile-menu">
		<div class="menu-backdrop"></div>
		<div class="close-btn"><span class="icon flaticon-multiply"></span></div>

		<nav class="menu-box">
			<div class="nav-logo"><a href="index.html"><img src="{{asset('front/assets')}}/images/logo-small.png" alt=""
						title=""></a></div>
			<div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
			</div>
		</nav>
	</div><!-- End Mobile Menu -->

</header>
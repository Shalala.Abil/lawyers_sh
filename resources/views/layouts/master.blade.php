@include('layouts.header')
@yield('title')

<body class="hidden-bar-wrapper">

<div class="page-wrapper">
 	
    @yield('content')

</div>
@include('layouts.footer')
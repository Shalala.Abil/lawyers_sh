@extends('layouts.master')



<style>
    .gradient-custom-3 {
        /* fallback for old browsers */
        background: #84fab0;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right, rgba(132, 250, 176, 0.5), rgba(143, 211, 244, 0.5));

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, rgba(132, 250, 176, 0.5), rgba(143, 211, 244, 0.5))
    }

    .gradient-custom-4 {
        /* fallback for old browsers */
        background: #84fab0;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right, rgba(132, 250, 176, 1), rgba(143, 211, 244, 1));

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, rgba(132, 250, 176, 1), rgba(143, 211, 244, 1))
    }
</style>


@section('content')


<section class="vh-100 bg-image"
    style="background-image: url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp');">
    <div class="mask d-flex align-items-center h-100 gradient-custom-3">
        <div style="margin-top:200px;" class="container h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="mt-5 col-12 col-md-9 col-lg-7 col-xl-6">
                    <div class="card" style="border-radius: 15px;">



                        <div class="card-body">
                            <div class="row justify-content-center mt-5">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">Verify Your Email Address</div>
                                        <div class="card-body">
                                            @if ($message = Session::get('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{ $message }}
                                            </div>
                                            @endif
                                            Before proceeding, please check your email for a verification link. If you
                                            did not receive the email,
                                            <form class="d-inline" method="POST"
                                                action="{{ route('verification.resend') }}">
                                                @csrf
                                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">click
                                                    here to request another</button>.
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection